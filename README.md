![cmake](https://img.shields.io/badge/14.0.0-cmakestandard-blue)
![gcc](https://img.shields.io/badge/7.5.0-gcc-red)
![gcc](https://img.shields.io/badge/14-c++-green)


## Documentation (ru)

### 1. Структура проекта
* _main.cpp_ - корень проекта;
* _test.cpp/test.h_ - хранят тесты классов;
* _Matrix.cpp / Matrix.h_ - базовый класс Матрицы;
* _exceptions/_ - директория исключений;
* _matrixTypes/_ - директория дочерних классов матрицы.

